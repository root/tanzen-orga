(set-face-attribute 'default nil :height 150)

(require 'package)
;; (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(load-theme 'solarized-dark t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages '(elfeed-summary solarized-theme elfeed)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

; edit server for browser firefox
(require 'edit-server)
(edit-server-start)

(setq text-quoting-style "grav")

;; Download Evil https://stackoverflow.com/questions/8483182/evil-mode-best-practice 
(unless (package-installed-p 'evil)
  (package-install 'evil))

; 
(add-hook 'org-mode-hook 'turn-on-flyspell)
(setq ispell-dictionary "german")

